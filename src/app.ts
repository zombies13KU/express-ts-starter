import express from "express";
import bodyParser from "body-parser";
import path from "path";
import flash from "express-flash";
import session from "express-session";

import * as homeController from "./controllers/home";

const app = express();

app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "../views"));
app.set("view engine", "pug");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: "itissecret"
}));
app.use(flash());
app.use(
    express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })
);

app.get("/", homeController.index);

export default app;